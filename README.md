# Render settings

<br>
Please use the link to access output images

https://www.dropbox.com/sh/nw1rmcffo92jlr2/AACK74Mg1h1xLWODP9OBMNPDa?dl=0

<br>
Check [benchmark_settings.xlsx](/benchmark_settings.xlsx) for Excel formatted version



|Item|Reflection amount|Material|Scene|Bump mapping|Part name|Scene reflectivity|Render time|Note|
|-|-|-|-|-|-|-|-|-|
|01_|0.15|sandblasted|3 point|1|Part4|-|39m|-|
|02_|0.15|sandblasted|white|1|Part4|-|42m|-|
|03_|0|sandblasted|white|1|Part4|-|31m|-|
|04_|0.5|sandblasted|white|1|Part4|-|44m|-|
|05_|0.15|polished aluminum|white|1|Part4|-|43m|-|
|06_|0|polished aluminum|white|1|Part4|default 2|35m|-|
|07_|0.15|polished aluminum|white|1|Part4 modified|default 2|83m|-|
|08_|0.15|polished aluminum|white|1|Part4 modified|1|83m|-|
|09_|0.5|polished aluminum|white|1|Part4 modified|1|N/A|-|
|10_|0.5|polished aluminum|white|1|Part4 modified|default 2|90m|Perspective|
|11_|0.15|light grey high gloss plasticd|white|1|Part4 modified|default 2|58m|-|
|12_|0.5|light grey high gloss plastic|white|1|Part4 modified|1|64m|-|
|13_|0.5|light grey low gloss plastic|white|1|Part4 modified|1|155m|blurry reflections on|
|14_|0.15|light grey low gloss plastic|white|1|Part4 modified|default 2|54m|-|
|15_|0.5|light grey low gloss plastic|white|1|Part4 modified|1|62m|-|
|16_|0.15|light grey low gloss plastic|white|1|Part4 modified|1|62m|rendering brightness 1|
|17_|0.1|pw-mt11150|white|0.07 default|Part4 modified|default 2|65m|-|
|18_|0.1|pw-mt11150|white|0.07 default|Part4 modified|1|65m|-|
|19_|0.02|clear thick glass|white|1|Part4 modified|default 2|77m|-|
|20_|-|wet concrete|white|1|Part4 modified|-|93m|-|
|21_|0.6|polished aluminum|white|1|Part4 modified|default 2|107m|decal|
|22_|0.05|light grey low gloss plastic|white|1|Part4 modified|default 2|56m|decal|
|23_|0.1|pw-mt11150|white|0.07 default|Part4 modified|default 2|156m|decal|
|24_|0.1|pw-mt11150|white|0.01|Part4 modified|default 2|39m|decal, specular spread 0.4, resolution half|
|25_|0.02|leather|white|0.25|Part4 modified|default 2|114m|decal. material defaults, width & height 102mm|
|26_|0.02|leather|white|0.1|Part4 modified|default 2|117m|specular amount 0.4|
|27_|0.1|leather|white|0.1|Part4 modified|default 2|171m|specular amount 0.6|
|28_|0.1|burlap|white|1|Part5|default 2|65m|mapping 50x50mm|
|29_|0.1|burlap|white|1|Part5|default 2|64m|mapping direction projection|




### Numeration

![](https://gitlab.com/fatihmehmetozcan/artec-eva-studio-benchmark/raw/master/Numeration.png)